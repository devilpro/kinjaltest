package com.imerchantech.kinjaltest;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.query.In;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by rohit on 7/18/17.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE_NAME = "test.db";
    private static final int DATABASE_VERSION = 1;

    private Dao<Info,Integer> infoIntegerDao =null ;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource,Info.class);
            Log.d(TAG, "onCreate: Table Create" );
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Info.class,false);
            onCreate(database,connectionSource);
            Log.d(TAG, "onUpgrade: Table drop");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public   Dao<Info,Integer> getInfoIntegerDao(){
        if(infoIntegerDao==null){
            try {
                infoIntegerDao = getDao(Info.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return infoIntegerDao;
    }

    public void cleartable(){
        try {
            TableUtils.clearTable(connectionSource,Info.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
