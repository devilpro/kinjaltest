package com.imerchantech.kinjaltest;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    RecyclerView recyclerView;

    private DatabaseHelper databaseHelper;
    private JsonArrayRequest request;
    private boolean isPresent = false;
    CustomAdapter customAdapter;
    SharedPreferences sharedpreferences;
    Context context;

    String url = "https://www.swiftpizza.com/AppCon/JsonDataPizza/8BE1NS0QS62ER/d2243fa4-cd79-d902-5e52-4ab131fef36b/ALL";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        sharedpreferences = getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        databaseHelper = new DatabaseHelper(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.e(TAG, "isPresent : " + sharedpreferences.getBoolean("isPresent", false));
        if (!sharedpreferences.getBoolean("isPresent", false)) {
            getInfoList();
        } else {
            List<Info> infos = new ArrayList<>();
            try {
                infos = databaseHelper.getInfoIntegerDao().queryForAll();
                Log.d(TAG, "get size of info list from database : " + infos.size());
            } catch (SQLException e) {
                e.printStackTrace();
            }

            CustomAdapter customAdapter = new CustomAdapter(context, infos);
            recyclerView.setAdapter(customAdapter);
        }
    }

    private void getInfoList() {
        request = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d(TAG, "onResponse: " + response);
                databaseHelper.cleartable();
                List<Info> infoList = new ArrayList<>();
                try {
                    for (int i = 0; i <= response.length(); i++) {
                        JSONObject object = (JSONObject) response.get(i);
                        String name = object.getString("name");
                        String img = object.getString("img");
                        Info info = new Info(name, img);

                        Log.d(TAG, "onResponse : " + name + "\t" + img);
                        infoList.add(info);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "infoList :  " + infoList.size());

                CustomAdapter customAdapter = new CustomAdapter(context, infoList);
                recyclerView.setAdapter(customAdapter);

                try {
                    databaseHelper.getInfoIntegerDao().create(infoList);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                isPresent = true;
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putBoolean("isPresent", isPresent);
                editor.commit();
            }
        }, new Response.ErrorListener()

        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "onErrorResponse: ", error);
            }
        });
        AppController.getInstance().addToRequestQueue(request);
    }
}
